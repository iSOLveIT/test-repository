from flask import Flask
from flask.views import MethodView


# instantiate flask
app = Flask(__name__)


# View for Index Route
class IndexEndpoint(MethodView):
    def get(self):
        return "
          <h1> Hello World. This is a flask app</h1>
        "


# Route for index
app.add_url_rule("/", view_func=IndexEndpoint.as_view("index"))


if __name__ == "__main__":
  app.secret_key = "Thisisssecret"
  app.run(port=5000, debug=True)


