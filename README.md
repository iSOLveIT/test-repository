# Simple Flask App

## Installation

```
pip install Flask
python app.py
```

## Description
```
This is an app for testing.
```

## Implementation
```
This project is implemented using Python-Flask, and is a REST API for test app.
```
